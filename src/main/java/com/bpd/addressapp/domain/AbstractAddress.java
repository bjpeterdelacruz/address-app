package com.bpd.addressapp.domain;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Addresses that contain at least all the following fields should extend this class:
 * <ul>
 * <li>Street 1</li>
 * <li>Street 2</li>
 * <li>City</li>
 * <li>State</li>
 * <li>Zip Code</li>
 * <li>Country</li>
 * </ul>
 * 
 * @author BJ Peter DeLaCruz
 */
public abstract class AbstractAddress implements Address {

  private static final long serialVersionUID = 1L;

  private String street1;

  private String street2;

  private String city;

  private String state;

  private String zipCode;

  private String country;

  /**
   * @return The first street.
   */
  public String getStreet1() {
    return street1;
  }

  /**
   * @return The second street.
   */
  public String getStreet2() {
    return street2;
  }

  /**
   * @return The city.
   */
  public String getCity() {
    return city;
  }

  /**
   * @return The state.
   */
  public String getState() {
    return state;
  }

  /**
   * @return The zip code.
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * @return The country.
   */
  public String getCountry() {
    return country;
  }

  /**
   * @param street1 The first street.
   */
  public void setStreet1(String street1) {
    this.street1 = street1;
  }

  /**
   * @param street2 The second street.
   */
  public void setStreet2(String street2) {
    this.street2 = street2;
  }

  /**
   * @param city The city.
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * @param state The state.
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * @param zipCode The zip code.
   */
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   * @param country The country.
   */
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * @return A stream containing all the fields in this address.
   */
  public Stream<String> toStream() {
    return Arrays.asList(street1, street2, city, state, zipCode, country).stream();
  }

}
