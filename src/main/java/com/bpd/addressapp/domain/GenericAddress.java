package com.bpd.addressapp.domain;

/**
 * Represents a generic, all-purpose address, useful for both businesses and individuals.
 * 
 * @author BJ Peter DeLaCruz
 */
public class GenericAddress extends AbstractAddress {

  private static final long serialVersionUID = 1L;

  /** {@inheritDoc} */
  @Override
  public String toString() {
    return "GenericAddress[street1=" + getStreet1() + ", street2=" + getStreet2() + ", city="
        + getCity() + ", state=" + getState() + ", zipCode=" + getZipCode() + ", country="
        + getCountry() + "]";
  }

}
