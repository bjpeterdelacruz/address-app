package com.bpd.addressapp.domain;

import java.io.Serializable;

/**
 * Represents an address.
 * 
 * @author BJ Peter DeLaCruz
 */
public interface Address extends Serializable {

}
