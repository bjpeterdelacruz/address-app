package com.bpd.addressapp.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * This utility class contains methods for sanitizing and validating addresses.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class AddressUtils {

  /** Private constructor. */
  private AddressUtils() {
    // Empty constructor
  }

  /**
   * Strips all HTML code from each string in the given stream to prevent cross-site scripting (XSS)
   * attacks.
   * 
   * @param inputStream A stream of strings from which to strip HTML code.
   * @return A stream of strings without any HTML code.
   */
  public static Stream<String> stripHtml(Stream<String> inputStream) {
    if (inputStream == null) {
      throw new IllegalArgumentException("stream is null");
    }

    List<String> output = inputStream.map(string -> Jsoup.clean(string, Whitelist.none()))
        .collect(Collectors.toCollection(ArrayList::new));

    return output.stream();
  }

  /**
   * Validates the given address. Checks if:
   * <ul>
   * <li>The length of each field is less than 255</li>
   * <li>The address is Hawaii specific</li>
   * </ul>
   * 
   * @param address The address to validate.
   * @return True if all the above conditions are true, false otherwise.
   */
  public static boolean validateAddress(AbstractAddress address) {
    return validateLengths(address) && isHawaiiSpecific(address);
  }

  /**
   * Tests whether the length of each field in the given address is less than or equal to 255
   * characters.
   * 
   * @param address The address to validate.
   * @return True if the length of each field is less than or equal to 255, false otherwise.
   */
  static boolean validateLengths(AbstractAddress address) {
    if (address == null) {
      return false;
    }

    return address.toStream().allMatch(string -> string != null && string.length() <= 255)
        && address.getZipCode().length() == 5;
  }

  /**
   * Tests whether the given address is Hawaii specific. An address is Hawaii specific if all of the
   * following are true:
   * <ul>
   * <li>the Country field is <code>USA</code></li>
   * <li>the Zip Code field is <code>96706</code>, <code>96734</code>, or <code>96813</code></li>
   * </ul>
   * 
   * @param address The address to validate.
   * @return True if the address is Hawaii specific, false otherwise.
   */
  static boolean isHawaiiSpecific(AbstractAddress address) {
    if (address == null) {
      return false;
    }

    List<Predicate<AbstractAddress>> tests = new ArrayList<>();

    tests.add(addr -> addr.getCountry() != null && addr.getCountry().equalsIgnoreCase("usa"));

    tests.add(addr -> {

      int zip;
      try {
        zip = Integer.parseInt(addr.getZipCode());
      }
      catch (NumberFormatException nfe) {
        return false;
      }

      return zip == 96706 || zip == 96734 || zip == 96813;

    });

    Predicate<AbstractAddress> test = tests.stream().reduce(Predicate::and).orElse(t -> true);

    return test.test(address);
  }

}
