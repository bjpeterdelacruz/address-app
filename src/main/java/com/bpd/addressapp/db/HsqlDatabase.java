package com.bpd.addressapp.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.hsqldb.jdbc.JDBCDataSource;

/**
 * Represents a HyperSQL Database (HSQLDB) that is used to store data for this web application.<br>
 * <br>
 * In <code>config.properties</code>, the <code>db_path</code> property can be configured so the
 * application can connect to a database running locally or remotely. For example:<br>
 * <br>
 * <ul>
 * <li><code>db_path=file:~/addressdb</code> | Creates a database called <code>addressdb</code> on
 * the local file system in the user's home directory</li>
 * <li><code>db_path=hsql://localhost/</code> | Connects to a database server running on
 * <code>localhost</code></code></li>
 * </ul>
 * 
 * @author BJ Peter DeLaCruz
 */
public class HsqlDatabase implements Database {

  private static final Logger LOGGER = Logger.getGlobal();

  private Connection conn;

  private static final String BUNDLE_NAME = "com/bpd/addressapp/db/config";

  private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

  private DataSource dataSource;

  /** {@inheritDoc} */
  @Override
  public void start() {
    String dbPath = getProperty("db_path");
    String db = "jdbc:hsqldb:" + dbPath + ";hsqldb.write_delay=false";

    dataSource = new JDBCDataSource();
    ((JDBCDataSource) dataSource).setDatabase(db);

    try {
      conn = dataSource.getConnection();
    }
    catch (SQLException e) {
      throw new IllegalStateException(e);
    }

    try {
      createTableIfNoneExists();
    }
    catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void stop() {
    try (Statement stmt = conn.createStatement()) {
      stmt.execute("SHUTDOWN");
      conn.close();
    }
    catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  /** {@inheritDoc} */
  @Override
  public DataSource getDataSource() {
    return dataSource;
  }

  /**
   * Gets the value for the given key in <code>config.properties</code>.
   * 
   * @param key The key.
   * @return The value for the key.
   * @throws MissingResourceException If problems are encountered while trying to get the value.
   */
  static String getProperty(String key) throws MissingResourceException {
    return RESOURCE_BUNDLE.getString(key);
  }

  /**
   * Creates a table called Addresses in the database if none already exists. Does nothing if a
   * table already exists.
   * 
   * @throws SQLException If there are problems creating the table.
   */
  private void createTableIfNoneExists() throws SQLException {
    try (PreparedStatement stmt = conn.prepareStatement(SqlCommands.CREATE_TABLE)) {
      stmt.execute();
    }
    catch (SQLSyntaxErrorException sql) {
      if (!sql.getMessage().contains("object name already exists")) {
        stop();
        throw sql;
      }
      LOGGER.log(Level.INFO, "Addresses table already exists.");
    }
    catch (SQLException sql) {
      LOGGER.log(Level.SEVERE, sql.getMessage(), sql);
      stop();
      throw sql;
    }
  }

}
