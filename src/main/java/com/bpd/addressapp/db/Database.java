package com.bpd.addressapp.db;

import javax.sql.DataSource;

/**
 * Represents a database.
 * 
 * @author BJ Peter DeLaCruz
 */
public interface Database {

  /**
   * Starts the database.
   */
  void start();

  /**
   * Stops the database.
   */
  void stop();

  /**
   * Returns the data source used by this database.
   * 
   * @return The data source.
   */
  DataSource getDataSource();

}
