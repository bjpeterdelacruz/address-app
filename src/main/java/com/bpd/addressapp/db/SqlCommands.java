package com.bpd.addressapp.db;

/**
 * This class contains String constants that represent SQL commands.
 * 
 * @author BJ Peter DeLaCruz
 */
public final class SqlCommands {

  /**
   * Private constructor.
   */
  private SqlCommands() {
    // Empty constructor
  }

  /**
   * SQL command used to create a table called Addresses.
   */
  public static final String CREATE_TABLE = "CREATE TABLE Addresses (" + "id INTEGER IDENTITY, "
      + "street1 VARCHAR(255), " + "street2 VARCHAR(255), " + "city VARCHAR(255), "
      + "state VARCHAR(255), " + "zipCode INTEGER, " + "country VARCHAR(255)" + ");";

  /**
   * SQL command used to insert an address into the Addresses table.
   */
  public static final String INSERT_ADDRESS = "INSERT INTO Addresses "
      + "(street1, street2, city, state, zipCode, country) VALUES (?, ?, ?, ?, ?, ?)";

}
