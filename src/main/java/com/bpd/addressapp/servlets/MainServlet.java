package com.bpd.addressapp.servlets;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.bpd.addressapp.dao.GenericAddressDao;
import com.bpd.addressapp.db.Database;
import com.bpd.addressapp.db.HsqlDatabase;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * The main servlet for this web application. When the user goes to "/", a web page containing a
 * form will be displayed. The user can enter a Hawaii specific address on the form and then submit
 * it. The fields in the address will be stripped of HTML and validated against criteria before it
 * either gets rejected or inserted into the database. When the user goes to "/addresses", a list of
 * all Hawaii specific addresses that are stored in the database will be displayed.
 * 
 * @author BJ Peter DeLaCruz
 */
public class MainServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  private final transient Database db = new HsqlDatabase();
  private transient GenericAddressDao dao = new GenericAddressDao();

  private static final Object LOCK = new Object();

  /** {@inheritDoc} */
  @Override
  public void init(ServletConfig config) throws ServletException {
    db.start();
    dao.setJdbcTemplate(db.getDataSource());
  }

  /** {@inheritDoc} */
  @Override
  public void destroy() {
    db.stop();
  }

  /** {@inheritDoc} */
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    if (req.getRequestURI().endsWith("addresses")) {
      synchronized (LOCK) {
        req.setAttribute("addresses", dao.getAllAddresses());
      }
      req.getRequestDispatcher("/WEB-INF/views/addresses.jsp").forward(req, resp);
    }
    else {
      req.setAttribute("address", new GenericAddress());
      req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    String street1 = req.getParameter("street1");
    String street2 = req.getParameter("street2");
    String city = req.getParameter("city");
    String state = req.getParameter("state");
    String zipCode = req.getParameter("zipCode");
    String country = req.getParameter("country");

    GenericAddress address = new GenericAddress();
    address.setStreet1(street1);
    address.setStreet2(street2);
    address.setCity(city);
    address.setState(state);
    address.setZipCode(zipCode);
    address.setCountry(country);

    synchronized (LOCK) {
      req.setAttribute("success", dao.addAddress(address));
    }
    req.getRequestDispatcher("/WEB-INF/views/index.jsp").forward(req, resp);
  }

  /**
   * Sets the DAO for inserting and retrieving addresses.
   * 
   * @param dao An AddressDao object.
   */
  void setAddressDao(GenericAddressDao dao) {
    this.dao = dao;
  }

}
