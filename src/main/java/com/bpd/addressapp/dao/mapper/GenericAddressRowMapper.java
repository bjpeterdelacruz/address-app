package com.bpd.addressapp.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * This class contains a method that will map a row of data to a ResultSet so the data can be
 * extracted to create an Address.
 * 
 * @author BJ Peter DeLaCruz
 */
public class GenericAddressRowMapper implements RowMapper<GenericAddress> {

  /** {@inheritDoc} */
  @Override
  public GenericAddress mapRow(ResultSet rs, int rowNum) throws SQLException {
    GenericAddressResultSetExtractor extractor = new GenericAddressResultSetExtractor();
    return extractor.extractData(rs);
  }

}
