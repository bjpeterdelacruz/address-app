package com.bpd.addressapp.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * This class contains a method that will extract all the fields from a ResultSet to create a
 * GenericAddress.
 * 
 * @author BJ Peter DeLaCruz
 */
public class GenericAddressResultSetExtractor implements ResultSetExtractor<GenericAddress> {

  /**
   * Extracts all the fields from the given ResultSet to create a GenericAddress.
   * 
   * @return An instance of GenericAddress.
   */
  @Override
  public GenericAddress extractData(ResultSet rs) throws SQLException, DataAccessException {

    GenericAddress address = new GenericAddress();
    address.setStreet1(rs.getString(2));
    address.setStreet2(rs.getString(3));
    address.setCity(rs.getString(4));
    address.setState(rs.getString(5));
    address.setZipCode(rs.getString(6));
    address.setCountry(rs.getString(7));

    return address;
  }

}
