/**
 * This package contains mappers for data access objects (DAOs).
 */
package com.bpd.addressapp.dao.mapper;
