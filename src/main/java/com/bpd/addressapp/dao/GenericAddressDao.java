package com.bpd.addressapp.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.bpd.addressapp.dao.mapper.GenericAddressRowMapper;
import com.bpd.addressapp.db.SqlCommands;
import com.bpd.addressapp.domain.AddressUtils;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * A Data Access Object (DAO) used to insert and retrieve addresses in the database.
 * 
 * @author BJ Peter DeLaCruz
 */
public class GenericAddressDao {

  private JdbcTemplate select;

  private static final Logger LOGGER = Logger.getGlobal();

  /**
   * Sets the JDBC template that will be used to insert addresses into and retrieve addresses from
   * the database.
   * 
   * @param dataSource The data source.
   */
  public void setJdbcTemplate(DataSource dataSource) {
    select = new JdbcTemplate(dataSource);
  }

  /**
   * Sets the JDBC template that will be used to insert addresses into and retrieve addresses from
   * the database.
   * 
   * @param select The JDBC template.
   */
  public void setJdbcTemplate(JdbcTemplate select) {
    this.select = select;
  }

  /**
   * Returns a list of all addresses in the database.
   * 
   * @return A list of addresses.
   */
  public List<GenericAddress> getAllAddresses() {
    return select.query("SELECT * FROM Addresses", new GenericAddressRowMapper());
  }

  /**
   * Inserts the given address into the database. First, the address will be stripped of HTML to
   * prevent XSS attacks and validated before it gets inserted.
   * 
   * @param address The address to insert into the database.
   * @return True if the insertion was successful, false otherwise.
   */
  public boolean addAddress(GenericAddress address) {
    if (address == null) {
      return false;
    }

    GenericAddress addr = new GenericAddress();
    try {
      Object[] fields = AddressUtils.stripHtml(address.toStream()).toArray();
      addr.setStreet1(fields[0].toString());
      addr.setStreet2(fields[1].toString());
      addr.setCity(fields[2].toString());
      addr.setState(fields[3].toString());
      addr.setZipCode(fields[4].toString());
      addr.setCountry(fields[5].toString());
    }
    catch (Exception e) {
      LOGGER.log(Level.SEVERE, "Invalid address: " + address.toString());
      return false;
    }

    if (!AddressUtils.validateAddress(addr)) {
      LOGGER.log(Level.WARNING, "Invalid address: " + addr.toString());
      return false;
    }

    Object[] fields = new Object[6];
    fields[0] = addr.getStreet1();
    fields[1] = addr.getStreet2();
    fields[2] = addr.getCity();
    fields[3] = addr.getState();
    fields[4] = Integer.parseInt(addr.getZipCode());
    fields[5] = addr.getCountry();

    return select.update(SqlCommands.INSERT_ADDRESS, fields) > 0;
  }

}
