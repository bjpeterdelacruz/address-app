<html>
<head>
    <title>Address App</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="http://www.bjpeterdelacruz.com/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
    <script language="javascript" type="text/javascript">
       function displayMessage() {
          var v = document.getElementById("result").innerText;
          if (v != "") {
             if (v === 'true') {
                alert("Your Hawaii specific address has been saved to the database!");
             }
             else {
                alert("Your address is not Hawaii specific. Country must be USA and zip code must be 96706, 96734, or 96813.");
             }
          }
       }
    </script>
</head>
<body onload="displayMessage();">
    <div class="col-sm-12" style="text-align: center; background-color: #009E7C">
        <h1 style="color: #FFFFFF; font-weight: bold">Address App</h1>
    </div>
    <div class="col-sm-6" style="text-align: center; vertical-align: center">
        <h3>
        You can add your Hawaii specific address to the database by entering your information
        in the fields.
        <br><br>
        A Hawaii specific address is an address that is located in the USA and in the zip codes
        96706, 96734, or 96813.
        </h3>
    </div>
    <div class="col-sm-6" style="text-align: center; margin-top:20px;">
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <label for="inputStreet1" class="col-sm-5 control-label">Street 1</label>
            <div class="col-sm-5">
                <input type="text" name="street1">
            </div>
        </div>
        <div class="form-group">
            <label for="inputStreet2" class="col-sm-5 control-label">Street 2</label>
            <div class="col-sm-5">
                <input type="text" name="street2">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCity" class="col-sm-5 control-label">City</label>
            <div class="col-sm-5">
                <input type="text" name="city">
            </div>
        </div>
        <div class="form-group">
            <label for="inputState" class="col-sm-5 control-label">State</label>
            <div class="col-sm-5">
                <input type="text" name="state">
            </div>
        </div>
        <div class="form-group">
            <label for="inputZipCode" class="col-sm-5 control-label">Zip Code</label>
            <div class="col-sm-5">
                <input type="number" name="zipCode">
            </div>
        </div>
        <div class="form-group">
            <label for="inputCountry" class="col-sm-5 control-label">Country</label>
            <div class="col-sm-5">
                <input type="text" name="country">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-default">Submit</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </div>
        </div>
        <div class="col-sm-12" style="text-align: center">
            <h4><a href="${pageContext.request.contextPath}/addresses"/>View My Addresses</a></h4>
        </div>
    </form>
    </div>
    <span id="result" style="display:none">${success}</span>
</body>
</html>
