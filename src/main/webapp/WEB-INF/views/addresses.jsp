<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Address App</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="http://www.bjpeterdelacruz.com/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="col-sm-12" style="text-align: center; background-color: #009E7C">
        <h1 style="color: #FFFFFF; font-weight: bold">Address App</h1>
    </div>
    <c:choose>
        <c:when test="${empty addresses}">
            <h4 class="col-sm-12" style="text-align: center">There are no addresses in the database.</h4>
            <h4 class="col-sm-12" style="text-align: center"><a href="${pageContext.request.contextPath}">Back</a></h4>
        </c:when>
        <c:otherwise>
            <h4 class="col-sm-12" style="text-align: center"><a href="${pageContext.request.contextPath}">Back</a></h4>
            <hr class="col-sm-12">
            <c:forEach items="${addresses}" var="address">
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">Street 1</h4>
                <h4 class="col-sm-6">${empty address.street1? '(blank)' : address.street1}</h4>
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">Street 2</h4>
                <h4 class="col-sm-6">${empty address.street2? '(blank)' : address.street2}</h4>
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">City</h4>
                <h4 class="col-sm-6">${empty address.city? '(blank)' : address.city}</h4>
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">State</h4>
                <h4 class="col-sm-6">${empty address.state? '(blank)' : address.state}</h4>
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">Zip Code</h4>
                <h4 class="col-sm-6">${empty address.zipCode? '(blank)' : address.zipCode}</h4>
                <h4 class="col-sm-6" style="text-align: right; color: #009E7C">Country</h4>
                <h4 class="col-sm-6">${empty address.country? '(blank)' : address.country}</h4>
                <hr class="col-sm-12">
            </c:forEach>
            <h4 class="col-sm-12" style="text-align: center"><a href="${pageContext.request.contextPath}">Back</a></h4>
        </c:otherwise>
    </c:choose>
</body>
</html>
