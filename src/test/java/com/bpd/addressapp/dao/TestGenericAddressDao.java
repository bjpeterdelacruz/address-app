package com.bpd.addressapp.dao;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.bpd.addressapp.db.SqlCommands;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * Unit tests for the {@link GenericAddressDao} class.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestGenericAddressDao {

  private static final String TEMP_PATH;
  private static final String JDBC_URL;

  static {
    try {
      TEMP_PATH = Files.createTempDirectory("temp").toString();
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    JDBC_URL =
        "jdbc:hsqldb:file:" + TEMP_PATH + File.pathSeparator + "addressdb;hsqldb.write_delay=false";
  }

  private static final String JDBC_DRIVER = org.hsqldb.jdbcDriver.class.getName();
  private static final String USER = "sa";
  private static final String PASSWORD = "";
  private static JDBCDataSource dataSource;
  private static Connection conn;

  /**
   * Sets up the database schema before tests are run.
   * 
   * @throws Exception If there are problems setting up the database.
   */
  @BeforeClass
  public static void createSchema() throws Exception {
    dataSource = new JDBCDataSource();
    dataSource.setDatabase(JDBC_URL);
    conn = dataSource.getConnection();
    PreparedStatement stmt = conn.prepareStatement(SqlCommands.CREATE_TABLE);
    stmt.execute();
  }

  /**
   * Imports the test data into the database.
   * 
   * @throws Exception If there are problems during the import process.
   */
  @Before
  public void importDataSet() throws Exception {
    Logger.getGlobal().setUseParentHandlers(false);
    IDataSet dataSet = readDataSet();
    cleanlyInsert(dataSet);
  }

  /**
   * Creates the builder that will read the data to import into the database from an XML file.
   * 
   * @return The builder used to read the data.
   * @throws Exception If there are problems creating the builder.
   */
  private IDataSet readDataSet() throws Exception {
    String path = "com\\bpd\\addressapp\\dao\\addresses.xml";
    File file = new File(getClass().getClassLoader().getResource(path).getPath());
    return new FlatXmlDataSetBuilder().build(file);
  }

  /**
   * Cleanly inserts data into the database by first deleting everything that is currently in the
   * database and then inserting the data.
   * 
   * @param dataSet The data to insert.
   * @throws Exception If there are problems deleting or inserting data.
   */
  private void cleanlyInsert(IDataSet dataSet) throws Exception {
    IDatabaseTester databaseTester = new JdbcDatabaseTester(JDBC_DRIVER, JDBC_URL, USER, PASSWORD);
    databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
    databaseTester.setDataSet(dataSet);
    databaseTester.onSetup();
  }

  /**
   * Tests whether the three addresses that were in the XML file were inserted into the database.
   */
  @Test
  public void shouldReturnThreeAddresses() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    List<GenericAddress> addresses = dao.getAllAddresses();
    assertThat(addresses.size()).isEqualTo(3);
  }

  /**
   * Tests whether addresses with invalid zip codes are not inserted into the database.
   */
  @Test
  public void shouldNotInsertAddressesWithInvalidZipCode() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    IntPredicate invalidZipCode = zip -> zip != 96706 && zip != 96734 && zip != 96813;
    IntStream.rangeClosed(96000, 97000).filter(invalidZipCode).forEach(zip -> {
      GenericAddress address = new GenericAddress();
      address.setStreet1("street1");
      address.setStreet2("street2");
      address.setCity("city");
      address.setState("state");
      address.setZipCode(Integer.toString(zip));
      address.setCountry("USA");
      assertThat(dao.addAddress(address)).isFalse();
    });
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);
  }

  /**
   * Tests whether addresses with invalid zip codes are not inserted into the database.
   */
  @Test
  public void shouldNotInsertAddressesWithInvalidCountries() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    IntPredicate validZipCode = zip -> zip == 96706 || zip == 96734 || zip != 96813;
    List<String> invalidCountries =
        Arrays.asList("China", "Japan", "South Korea", "Taiwan", "Vietnam");
    IntStream.rangeClosed(96000, 97000).filter(validZipCode).forEach(zip -> {
      invalidCountries.forEach(country -> {
        GenericAddress address = new GenericAddress();
        address.setStreet1("street1");
        address.setStreet2("street2");
        address.setCity("city");
        address.setState("state");
        address.setZipCode(Integer.toString(zip));
        address.setCountry(country);
        assertThat(dao.addAddress(address)).isFalse();
      });
    });
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);
  }

  /**
   * Tests whether addresses with valid zip codes and countries are inserted into the database.
   */
  @Test
  public void shouldInsertAddressesWithValidZipCodeAndValidCountry() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    IntPredicate validZipCode = zip -> zip == 96706 || zip == 96734 || zip == 96813;
    IntStream.rangeClosed(96000, 97000).filter(validZipCode).forEach(zip -> {
      GenericAddress address = new GenericAddress();
      address.setStreet1("street1");
      address.setStreet2("street2");
      address.setCity("city");
      address.setState("state");
      address.setZipCode(Integer.toString(zip));
      address.setCountry("USA");
      assertThat(dao.addAddress(address)).isTrue();
    });
    assertThat(dao.getAllAddresses().size()).isEqualTo(6);
  }

  /**
   * Tests whether addresses with invalid zip codes and countries are not inserted into the
   * database.
   */
  @Test
  public void shouldNotInsertAddressesWithInvalidZipCodeAndInvalidCountry() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    IntPredicate invalidZipCode = zip -> zip != 96706 && zip != 96734 && zip != 96813;
    List<String> invalidCountries =
        Arrays.asList("China", "Japan", "South Korea", "Taiwan", "Vietnam");
    IntStream.rangeClosed(96000, 97000).filter(invalidZipCode).forEach(zip -> {
      invalidCountries.forEach(country -> {
        GenericAddress address = new GenericAddress();
        address.setStreet1("street1");
        address.setStreet2("street2");
        address.setCity("city");
        address.setState("state");
        address.setZipCode(Integer.toString(zip));
        address.setCountry(country);
        assertThat(dao.addAddress(address)).isFalse();
      });
    });
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);
  }

  /**
   * Tests whether an address with null fields is not inserted into the database.
   */
  @Test
  public void shouldNotInsertAddressWithNullFields() {
    GenericAddressDao dao = new GenericAddressDao();
    dao.setJdbcTemplate(dataSource);
    assertThat(dao.addAddress(null)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    assertThat(dao.addAddress(new GenericAddress())).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    GenericAddress address = new GenericAddress();
    address.setStreet1("abc");
    assertThat(dao.addAddress(address)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    address.setStreet2("def");
    assertThat(dao.addAddress(address)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    address.setCity("ghi");
    assertThat(dao.addAddress(address)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    address.setState("jkl");
    assertThat(dao.addAddress(address)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);

    address.setZipCode("mno");
    assertThat(dao.addAddress(address)).isFalse();
    assertThat(dao.getAllAddresses().size()).isEqualTo(3);
  }

  /**
   * Shuts down the database, closes the connection to it, and deletes the temporary directory that
   * contains the database.
   * 
   * @throws Exception If there are problems during the cleanup process.
   */
  @AfterClass
  public static void cleanUp() throws Exception {
    Statement stmt = conn.createStatement();
    stmt.execute("SHUTDOWN");
    conn.close();
    Files.delete(Paths.get(TEMP_PATH));
  }

}
