package com.bpd.addressapp.servlets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import com.bpd.addressapp.dao.GenericAddressDao;
import com.bpd.addressapp.db.SqlCommands;
import com.bpd.addressapp.domain.GenericAddress;

/**
 * Unit tests for the {@link MainServlet} class.
 * 
 * @author BJ Peter DeLaCruz
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(MainServlet.class)
public class TestMainServlet {

  private MainServlet servlet;
  @Mock
  private GenericAddressDao dao;
  @Mock
  private JDBCDataSource dataSource;

  /**
   * Sets up the unit tests.
   */
  @Before
  public void setup() {
    Logger.getGlobal().setUseParentHandlers(false);
    servlet = new MainServlet();
    servlet.setAddressDao(dao);
  }

  /**
   * Tests the doGet() method.
   * 
   * @throws Exception If problems are encountered during the test.
   */
  @Test
  public void testDoGet1() throws Exception {
    List<GenericAddress> expected = createAddresses();

    PowerMockito.when(dao.getAllAddresses()).thenReturn(expected);

    MockHttpServletRequest request = new MockHttpServletRequest();
    assertThat(request.getAttribute("addresses")).isNull();

    request.setRequestURI("/book/of/addresses");
    servlet.doGet(request, new MockHttpServletResponse());

    assertThat(request.getAttribute("addresses")).isEqualTo(expected);
  }

  /**
   * Tests the doGet() method.
   * 
   * @throws Exception If problems are encountered during the test.
   */
  @Test
  public void testDoGet2() throws Exception {
    MockHttpServletRequest request = new MockHttpServletRequest();
    assertThat(request.getAttribute("address")).isNull();

    request.setRequestURI("/");
    servlet.doGet(request, new MockHttpServletResponse());

    assertThat(request.getAttribute("address")).isInstanceOf(GenericAddress.class);
  }

  /**
   * Tests the doPost() method.
   * 
   * @throws Exception If problems are encountered during the test.
   */
  @Test
  public void testDoPost() throws Exception {
    GenericAddressDao dao = new GenericAddressDao();

    JdbcTemplate source = PowerMockito.mock(JdbcTemplate.class);
    PowerMockito.when(source.update(SqlCommands.INSERT_ADDRESS, createFields(96706))).thenReturn(1);
    PowerMockito.when(source.update(SqlCommands.INSERT_ADDRESS, createFields(96734))).thenReturn(1);
    PowerMockito.when(source.update(SqlCommands.INSERT_ADDRESS, createFields(96813))).thenReturn(1);
    dao.setJdbcTemplate(source);

    MainServlet servlet = new MainServlet();
    servlet.setAddressDao(dao);

    List<String> countries =
        Arrays.asList("China", "Japan", "South Korea", "Taiwan", "USA", "Vietnam");
    IntStream.rangeClosed(96000, 97000).forEach(zip -> {
      countries.forEach(country -> {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("street1", "abc");
        request.setParameter("street2", "def");
        request.setParameter("city", "ghi");
        request.setParameter("state", "jkl");
        request.setParameter("zipCode", Integer.toString(zip));
        request.setParameter("country", country);

        try {
          assertThat(request.getAttribute("success")).isNull();
          servlet.doPost(request, new MockHttpServletResponse());
          boolean result = (Boolean) request.getAttribute("success");
          if ((zip == 96706 || zip == 96734 || zip == 96813) && "USA".equals(country)) {
            assertThat(result).isTrue();
          }
          else {
            assertThat(result).isFalse();
          }
        }
        catch (Exception e) {
          fail(e.getMessage());
        }
      });
    });
  }

  /**
   * Creates an array of fields for an address.
   * 
   * @param zip The zip code.
   * @return An array of fields.
   */
  private static Object[] createFields(int zip) {
    Object[] fields = new Object[6];
    fields[0] = "abc";
    fields[1] = "def";
    fields[2] = "ghi";
    fields[3] = "jkl";
    fields[4] = zip;
    fields[5] = "USA";
    return fields;
  }

  /**
   * Creates a list of valid addresses.
   * 
   * @return A list of valid addresses.
   */
  private static List<GenericAddress> createAddresses() {
    List<GenericAddress> addresses = new ArrayList<>();

    GenericAddress address = new GenericAddress();
    address.setZipCode("96706");
    address.setCountry("USA");
    addresses.add(address);

    address = new GenericAddress();
    address.setZipCode("96734");
    address.setCountry("USA");
    addresses.add(address);

    address = new GenericAddress();
    address.setZipCode("96813");
    address.setCountry("USA");
    addresses.add(address);

    return addresses;
  }

}
