package com.bpd.addressapp.domain;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

/**
 * Unit tests for the {@link GenericAddress} class.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestGenericAddress {

  /**
   * Tests the getters and setters in {@link GenericAddress}.
   */
  @Test
  public void testGettersSetters() {
    GenericAddress address = new GenericAddress();
    address.setStreet1("abc");
    assertThat(address.getStreet1()).isEqualTo("abc");

    address = new GenericAddress();
    address.setStreet2("def");
    assertThat(address.getStreet2()).isEqualTo("def");

    address = new GenericAddress();
    address.setCity("ghi");
    assertThat(address.getCity()).isEqualTo("ghi");

    address = new GenericAddress();
    address.setState("jkl");
    assertThat(address.getState()).isEqualTo("jkl");

    address = new GenericAddress();
    address.setZipCode("mno");
    assertThat(address.getZipCode()).isEqualTo("mno");

    address = new GenericAddress();
    address.setCountry("pqr");
    assertThat(address.getCountry()).isEqualTo("pqr");
  }

}
