package com.bpd.addressapp.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;
import org.junit.Test;

/**
 * Unit tests for the {@link AddressUtils} class.
 * 
 * @author BJ Peter DeLaCruz
 */
public class TestAddressUtils {

  /**
   * Tests the {@link AddressUtils#stripHtml(GenericAddress)} method.
   */
  @Test
  public void testStripHtml() {
    try {
      assertThat(AddressUtils.stripHtml(null));
      fail("An exception was supposed to be thrown here.");
    }
    catch (Exception e) {
      assertThat(e).hasMessage("stream is null");
    }

    String htmlText =
        "<p>An <a href='http://example.com/'><b>example</b></a> of <h1>a</h2> link.</p>";
    String expected = "An example of a link.";

    GenericAddress address = createAddress(htmlText, "def", "ghi", "jkl", "mno", "pqr");
    Object[] fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[0]).isEqualTo(expected);

    address = createAddress("abc", htmlText, "ghi", "jkl", "mno", "pqr");
    fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[1]).isEqualTo(expected);

    address = createAddress("abc", "def", htmlText, "jkl", "mno", "pqr");
    fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[2]).isEqualTo(expected);

    address = createAddress("abc", "def", "ghi", htmlText, "mno", "pqr");
    fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[3]).isEqualTo(expected);

    address = createAddress("abc", "def", "ghi", "jkl", htmlText, "pqr");
    fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[4]).isEqualTo(expected);

    address = createAddress("abc", "def", "ghi", "jkl", "mno", htmlText);
    fields = AddressUtils.stripHtml(address.toStream()).toArray();
    assertThat(fields[5]).isEqualTo(expected);
  }

  /**
   * Helper method to create an Address.
   * 
   * @param s1 Street 1.
   * @param s2 Street 2.
   * @param c City.
   * @param s State.
   * @param z Zip code.
   * @param cn Country.
   * @return An address.
   */
  private static GenericAddress createAddress(String s1, String s2, String c, String s, String z,
      String cn) {

    GenericAddress address = new GenericAddress();
    address.setStreet1(s1);
    address.setStreet2(s2);
    address.setCity(c);
    address.setState(s);
    address.setZipCode(z);
    address.setCountry(cn);

    return address;
  }

  /**
   * Tests the {@link AddressUtils#validateLengths(GenericAddress)} method.
   */
  @Test
  public void testValidateLengths() {
    assertThat(AddressUtils.validateLengths(null)).isFalse();

    Random random = new Random(System.currentTimeMillis());
    StringBuilder builder = new StringBuilder();
    IntStream.rangeClosed(0, 1000).forEach(i -> {
      builder.append(Integer.toString(random.nextInt(100)).charAt(0));
    });

    GenericAddress address = createAddress("abc", "def", "ghi", "jkl", "12345", "pqr");
    assertThat(AddressUtils.validateLengths(address)).isTrue();

    address = createAddress(builder.toString(), "abc", "def", "ghi", "12345", "mno");
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", builder.toString(), "def", "ghi", "12345", "mno");
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", "def", builder.toString(), "ghi", "12345", "mno");
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", "def", "ghi", builder.toString(), "12345", "mno");
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", "def", "ghi", "jkl", builder.toString(), "mno");
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", "def", "ghi", "jkl", "12345", builder.toString());
    assertThat(AddressUtils.validateLengths(address)).isFalse();

    address = createAddress("abc", "def", "ghi", "jkl", "123", "pqr");
    assertThat(AddressUtils.validateLengths(address)).isFalse();
  }

  /**
   * Tests the {@link AddressUtils#isHawaiiSpecific(GenericAddress)} method.
   */
  @Test
  public void testIsHawaiiSpecific() {
    assertThat(AddressUtils.isHawaiiSpecific(null)).isFalse();

    IntPredicate validZipCode = zip -> zip == 96706 || zip == 96734 || zip == 96813;
    IntStream.rangeClosed(96000, 97000).filter(validZipCode).forEach(zip -> {
      String zipCode = Integer.toString(zip);
      GenericAddress address =
          createAddress("155 Kapalulu Place", "", "Honolulu", "Hawaii", zipCode, "USA");
      assertThat(AddressUtils.isHawaiiSpecific(address)).isTrue();
    });

    IntPredicate invalidZipCode = zip -> zip != 96706 && zip != 96734 && zip != 96813;
    IntStream.rangeClosed(96000, 97000).filter(invalidZipCode).forEach(zip -> {
      String zipCode = Integer.toString(zip);
      GenericAddress address =
          createAddress("155 Kapalulu Place", "", "Honolulu", "Hawaii", zipCode, "USA");
      assertThat(AddressUtils.isHawaiiSpecific(address)).isFalse();
    });

    List<String> invalidCountries =
        Arrays.asList("China", "Japan", "South Korea", "Taiwan", "Vietnam");
    IntStream.rangeClosed(96000, 97000).filter(validZipCode).forEach(zip -> {
      invalidCountries.forEach(country -> {
        String zipCode = Integer.toString(zip);
        GenericAddress address =
            createAddress("155 Kapalulu Place", "", "Honolulu", "Hawaii", zipCode, country);
        assertThat(AddressUtils.isHawaiiSpecific(address)).isFalse();
      });
    });

    IntStream.rangeClosed(96000, 97000).filter(invalidZipCode).forEach(zip -> {
      invalidCountries.forEach(country -> {
        String zipCode = Integer.toString(zip);
        GenericAddress address =
            createAddress("155 Kapalulu Place", "", "Honolulu", "Hawaii", zipCode, country);
        assertThat(AddressUtils.isHawaiiSpecific(address)).isFalse();
      });
    });

    IntStream.rangeClosed(0, 9999).forEach(zip -> {
      String zipCode = Integer.toString(zip);
      GenericAddress address =
          createAddress("155 Kapalulu Place", "", "Honolulu", "Hawaii", zipCode, "USA");
      assertThat(AddressUtils.isHawaiiSpecific(address)).isFalse();
    });
  }

}
