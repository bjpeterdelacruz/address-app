CREATE TABLE Addresses (id INTEGER IDENTITY, street1 VARCHAR(255),
    street2 VARCHAR(255), city VARCHAR(255), state VARCHAR(255), zipCode INTEGER, country VARCHAR(255));

INSERT INTO Addresses (street1, street2, city, state, zipCode, country)
    VALUES ('Castle and Cooke', '155 Kapalulu Place', 'Honolulu', 'Hawaii', 96706, 'USA');
